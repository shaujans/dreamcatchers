using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Dreamcatchers.Db;
using Dreamcatchers.Models;
using Dreamcatchers.Models.Security;

namespace Dreamcatchers.API.Controllers
{
    [Route("api/[controller]")]
     public class PersonsController : BaseController 
     {       
        private const string FAILGETENTITIES = "Failed to get persons from the API";
        private const string FAILGETENTITYBYID = "Failed to get person from the API by Id: {0}";


        public PersonsController(ApplicationDbContext applicationDbContext, UserManager<ApplicationUser> userManager):base(applicationDbContext, userManager) 
        {
        }

        [HttpGet(Name = "GetAccomodations")]
        public async Task<IActionResult> GetAccomodations()
        {
            var model = await ApplicationDbContext.Accomodations.ToListAsync();
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITIES);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpGet("{AccomodationId:int}", Name = "GetAccomodationById")]
        public async Task<IActionResult> GetAccomodationById(Int64 AccomodationId)
        {
            var model = await ApplicationDbContext.Accomodations.FirstOrDefaultAsync(o => o.Id == AccomodationId);
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID, AccomodationId);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }
    }
}