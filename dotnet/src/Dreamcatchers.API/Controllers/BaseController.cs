using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Dreamcatchers.Db;
using Dreamcatchers.Models.Security;

namespace Dreamcatchers.API.Controllers {
    public class BaseController : Controller {
        protected ApplicationDbContext ApplicationDbContext { get; set; }
        protected UserManager<ApplicationUser> UserManager { get; set; }
        public BaseController(ApplicationDbContext applicationDbContext, UserManager<ApplicationUser> userManager) {
            ApplicationDbContext = applicationDbContext;
            UserManager = userManager;
        }
    }
}