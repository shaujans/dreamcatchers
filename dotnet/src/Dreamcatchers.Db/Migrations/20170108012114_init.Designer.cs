﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Dreamcatchers.Db;

namespace Dreamcatchers.Db.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20170108012114_init")]
    partial class init
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("Npgsql:PostgresExtension:.uuid-ossp", "'uuid-ossp', '', ''")
                .HasAnnotation("ProductVersion", "1.0.0-rtm-21431");

            modelBuilder.Entity("Dreamcatchers.Models.Accomodation", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("Description")
                        .IsRequired();

                    b.Property<int?>("HouseTypeId");

                    b.Property<long?>("LocationId");

                    b.Property<short>("MaxPersons");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 255);

                    b.Property<long?>("ProfileId");

                    b.Property<int?>("RentingTypeId");

                    b.Property<DateTime?>("UpdatedAt");

                    b.HasKey("Id");

                    b.HasIndex("HouseTypeId");

                    b.HasIndex("LocationId");

                    b.HasIndex("ProfileId");

                    b.HasIndex("RentingTypeId");

                    b.ToTable("Accomodations");
                });

            modelBuilder.Entity("Dreamcatchers.Models.AccomodationBathroomConfig", b =>
                {
                    b.Property<long>("AccomodationId");

                    b.Property<long>("BathroomConfigId");

                    b.HasKey("AccomodationId", "BathroomConfigId");

                    b.HasIndex("AccomodationId");

                    b.HasIndex("BathroomConfigId");

                    b.ToTable("AccomodationBathroomConfigs");
                });

            modelBuilder.Entity("Dreamcatchers.Models.AccomodationBedroomConfig", b =>
                {
                    b.Property<long>("AccomodationId");

                    b.Property<long>("BedroomConfigId");

                    b.HasKey("AccomodationId", "BedroomConfigId");

                    b.HasIndex("AccomodationId");

                    b.HasIndex("BedroomConfigId");

                    b.ToTable("AccomodationBedroomConfigs");
                });

            modelBuilder.Entity("Dreamcatchers.Models.AccomodationFacilityConfig", b =>
                {
                    b.Property<long>("AccomodationId");

                    b.Property<long>("FacilityConfigId");

                    b.HasKey("AccomodationId", "FacilityConfigId");

                    b.HasIndex("AccomodationId");

                    b.HasIndex("FacilityConfigId");

                    b.ToTable("AccomodationFacilityConfigs");
                });

            modelBuilder.Entity("Dreamcatchers.Models.AccomodationMedia", b =>
                {
                    b.Property<long>("AccomodationId");

                    b.Property<long>("MediaId");

                    b.HasKey("AccomodationId", "MediaId");

                    b.HasIndex("AccomodationId");

                    b.HasIndex("MediaId");

                    b.ToTable("AccomodationMedias");
                });

            modelBuilder.Entity("Dreamcatchers.Models.BathroomConfig", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("Shortname");

                    b.Property<DateTime?>("UpdatedAt");

                    b.HasKey("Id");

                    b.ToTable("BathroomConfigs");
                });

            modelBuilder.Entity("Dreamcatchers.Models.BedroomConfig", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<short>("MaxPersons");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("Shortname")
                        .HasAnnotation("MaxLength", 127);

                    b.Property<DateTime?>("UpdatedAt");

                    b.HasKey("Id");

                    b.ToTable("BedroomConfigs");
                });

            modelBuilder.Entity("Dreamcatchers.Models.Booking", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("AccomodationId");

                    b.Property<DateTime>("Arrival");

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("now()");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<DateTime>("Departure");

                    b.Property<decimal>("Price");

                    b.Property<long>("ProfileId");

                    b.Property<byte>("Status");

                    b.Property<DateTime?>("UpdatedAt")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasDefaultValueSql("now()");

                    b.HasKey("Id");

                    b.HasIndex("AccomodationId");

                    b.HasIndex("ProfileId");

                    b.ToTable("Bookings");
                });

            modelBuilder.Entity("Dreamcatchers.Models.Busy", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("AccomodationId");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<DateTime>("From");

                    b.Property<DateTime>("To");

                    b.Property<DateTime?>("UpdatedAt");

                    b.HasKey("Id");

                    b.HasIndex("AccomodationId");

                    b.ToTable("Busies");
                });

            modelBuilder.Entity("Dreamcatchers.Models.City", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("Name");

                    b.Property<long>("StateId");

                    b.Property<DateTime?>("UpdatedAt");

                    b.HasKey("Id");

                    b.HasIndex("StateId");

                    b.ToTable("Cities");
                });

            modelBuilder.Entity("Dreamcatchers.Models.Country", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Code");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("Name");

                    b.Property<DateTime?>("UpdatedAt");

                    b.HasKey("Id");

                    b.ToTable("Countries");
                });

            modelBuilder.Entity("Dreamcatchers.Models.FacilityCategorie", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<DateTime?>("UpdatedAt");

                    b.HasKey("Id");

                    b.ToTable("FacilityCategories");
                });

            modelBuilder.Entity("Dreamcatchers.Models.FacilityConfig", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<long?>("FacilityCategorieId");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("Shortname")
                        .HasAnnotation("MaxLength", 127);

                    b.Property<DateTime?>("UpdatedAt");

                    b.HasKey("Id");

                    b.HasIndex("FacilityCategorieId");

                    b.ToTable("FacilityConfigs");
                });

            modelBuilder.Entity("Dreamcatchers.Models.HouseType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 255);

                    b.Property<string>("ShortName")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 127);

                    b.Property<DateTime?>("UpdatedAt");

                    b.HasKey("Id");

                    b.ToTable("HouseTypes");
                });

            modelBuilder.Entity("Dreamcatchers.Models.Location", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("CityId");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<double>("Lat")
                        .HasAnnotation("MaxLength", 127);

                    b.Property<double>("Lon")
                        .HasAnnotation("MaxLength", 127);

                    b.Property<string>("Number");

                    b.Property<string>("Street");

                    b.Property<DateTime?>("UpdatedAt");

                    b.HasKey("Id");

                    b.HasIndex("CityId");

                    b.ToTable("Locations");
                });

            modelBuilder.Entity("Dreamcatchers.Models.Media", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("Name");

                    b.Property<short>("Type");

                    b.Property<DateTime?>("UpdatedAt");

                    b.Property<string>("Url");

                    b.HasKey("Id");

                    b.ToTable("Medias");
                });

            modelBuilder.Entity("Dreamcatchers.Models.Price", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("AccomodationId");

                    b.Property<float>("Cleaning");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<float>("Deposit");

                    b.Property<float>("PriceDay");

                    b.Property<float>("PriceWeek");

                    b.Property<float>("Service");

                    b.Property<DateTime?>("UpdatedAt");

                    b.HasKey("Id");

                    b.HasIndex("AccomodationId");

                    b.ToTable("Prices");
                });

            modelBuilder.Entity("Dreamcatchers.Models.Profile", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long?>("ApplicationUserProfileId");

                    b.Property<DateTime?>("Birthday");

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("now()");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 255);

                    b.Property<byte>("Gender");

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 255);

                    b.Property<DateTime?>("UpdatedAt")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasDefaultValueSql("now()");

                    b.HasKey("Id");

                    b.HasIndex("ApplicationUserProfileId")
                        .IsUnique();

                    b.ToTable("Profiles");
                });

            modelBuilder.Entity("Dreamcatchers.Models.ProfileTrip", b =>
                {
                    b.Property<long>("ProfileId");

                    b.Property<long>("TripId");

                    b.HasKey("ProfileId", "TripId");

                    b.HasIndex("ProfileId");

                    b.HasIndex("TripId");

                    b.ToTable("ProfileTrips");
                });

            modelBuilder.Entity("Dreamcatchers.Models.RentingType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 255);

                    b.Property<string>("Shortname")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 127);

                    b.Property<DateTime?>("UpdatedAt");

                    b.HasKey("Id");

                    b.ToTable("RentingTypes");
                });

            modelBuilder.Entity("Dreamcatchers.Models.Security.ApplicationRole", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("Name")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedName")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<DateTime?>("UpdatedAt");

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Dreamcatchers.Models.Security.ApplicationUser", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("now()");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("Email")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedUserName")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<long?>("ProfileId")
                        .IsRequired();

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<DateTime?>("UpdatedAt")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasDefaultValueSql("now()");

                    b.Property<string>("UserName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("Dreamcatchers.Models.State", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("CountryId");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("Name");

                    b.Property<DateTime?>("UpdatedAt");

                    b.HasKey("Id");

                    b.HasIndex("CountryId");

                    b.ToTable("States");
                });

            modelBuilder.Entity("Dreamcatchers.Models.Status", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("AccomodationId");

                    b.Property<string>("Content")
                        .IsRequired();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 255);

                    b.Property<DateTime?>("UpdatedAt");

                    b.HasKey("Id");

                    b.HasIndex("AccomodationId");

                    b.ToTable("Statuses");
                });

            modelBuilder.Entity("Dreamcatchers.Models.Trip", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("AccomodationId");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<DateTime>("From");

                    b.Property<decimal>("Price");

                    b.Property<DateTime>("To");

                    b.Property<DateTime?>("UpdatedAt");

                    b.HasKey("Id");

                    b.HasIndex("AccomodationId");

                    b.ToTable("Trips");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<System.Guid>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<Guid>("RoleId");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<System.Guid>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<Guid>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<System.Guid>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<Guid>("UserId");

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<System.Guid>", b =>
                {
                    b.Property<Guid>("UserId");

                    b.Property<Guid>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserToken<System.Guid>", b =>
                {
                    b.Property<Guid>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("OpenIddict.OpenIddictApplication<System.Guid>", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClientId");

                    b.Property<string>("ClientSecret");

                    b.Property<string>("DisplayName");

                    b.Property<string>("LogoutRedirectUri");

                    b.Property<string>("RedirectUri");

                    b.Property<string>("Type");

                    b.HasKey("Id");

                    b.HasIndex("ClientId")
                        .IsUnique();

                    b.ToTable("OpenIddictApplications");
                });

            modelBuilder.Entity("OpenIddict.OpenIddictAuthorization<System.Guid>", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Scope");

                    b.HasKey("Id");

                    b.ToTable("OpenIddictAuthorizations");
                });

            modelBuilder.Entity("OpenIddict.OpenIddictScope<System.Guid>", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.HasKey("Id");

                    b.ToTable("OpenIddictScopes");
                });

            modelBuilder.Entity("OpenIddict.OpenIddictToken<System.Guid>", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("ApplicationId");

                    b.Property<Guid?>("AuthorizationId");

                    b.Property<string>("Type");

                    b.HasKey("Id");

                    b.HasIndex("ApplicationId");

                    b.HasIndex("AuthorizationId");

                    b.ToTable("OpenIddictTokens");
                });

            modelBuilder.Entity("Dreamcatchers.Models.Accomodation", b =>
                {
                    b.HasOne("Dreamcatchers.Models.HouseType", "HouseType")
                        .WithMany("Accomodations")
                        .HasForeignKey("HouseTypeId");

                    b.HasOne("Dreamcatchers.Models.Location", "Location")
                        .WithMany("Accomodations")
                        .HasForeignKey("LocationId");

                    b.HasOne("Dreamcatchers.Models.Profile", "Profile")
                        .WithMany("Accomodations")
                        .HasForeignKey("ProfileId");

                    b.HasOne("Dreamcatchers.Models.RentingType", "RentingType")
                        .WithMany("Accomodations")
                        .HasForeignKey("RentingTypeId");
                });

            modelBuilder.Entity("Dreamcatchers.Models.AccomodationBathroomConfig", b =>
                {
                    b.HasOne("Dreamcatchers.Models.Accomodation", "Accomodation")
                        .WithMany("BathroomConfigs")
                        .HasForeignKey("AccomodationId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Dreamcatchers.Models.BathroomConfig", "BathroomConfig")
                        .WithMany("Accomodations")
                        .HasForeignKey("BathroomConfigId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Dreamcatchers.Models.AccomodationBedroomConfig", b =>
                {
                    b.HasOne("Dreamcatchers.Models.Accomodation", "Accomodation")
                        .WithMany("BedroomConfigs")
                        .HasForeignKey("AccomodationId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Dreamcatchers.Models.BedroomConfig", "BedroomConfig")
                        .WithMany("Accomodations")
                        .HasForeignKey("BedroomConfigId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Dreamcatchers.Models.AccomodationFacilityConfig", b =>
                {
                    b.HasOne("Dreamcatchers.Models.Accomodation", "Accomodation")
                        .WithMany("FacilityConfigs")
                        .HasForeignKey("AccomodationId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Dreamcatchers.Models.FacilityConfig", "FacilityConfig")
                        .WithMany("Accomodations")
                        .HasForeignKey("FacilityConfigId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Dreamcatchers.Models.AccomodationMedia", b =>
                {
                    b.HasOne("Dreamcatchers.Models.Accomodation", "Accomodation")
                        .WithMany("Medias")
                        .HasForeignKey("AccomodationId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Dreamcatchers.Models.Media", "Media")
                        .WithMany("Accomodations")
                        .HasForeignKey("MediaId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Dreamcatchers.Models.Booking", b =>
                {
                    b.HasOne("Dreamcatchers.Models.Accomodation", "Accomodation")
                        .WithMany("Bookings")
                        .HasForeignKey("AccomodationId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Dreamcatchers.Models.Profile", "Profile")
                        .WithMany("Bookings")
                        .HasForeignKey("ProfileId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Dreamcatchers.Models.Busy", b =>
                {
                    b.HasOne("Dreamcatchers.Models.Accomodation", "Accomodation")
                        .WithMany("Busies")
                        .HasForeignKey("AccomodationId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Dreamcatchers.Models.City", b =>
                {
                    b.HasOne("Dreamcatchers.Models.State", "State")
                        .WithMany("Cities")
                        .HasForeignKey("StateId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Dreamcatchers.Models.FacilityConfig", b =>
                {
                    b.HasOne("Dreamcatchers.Models.FacilityCategorie", "FacilityCategorie")
                        .WithMany("FacilityConfigs")
                        .HasForeignKey("FacilityCategorieId");
                });

            modelBuilder.Entity("Dreamcatchers.Models.Location", b =>
                {
                    b.HasOne("Dreamcatchers.Models.City", "City")
                        .WithMany("Locations")
                        .HasForeignKey("CityId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Dreamcatchers.Models.Price", b =>
                {
                    b.HasOne("Dreamcatchers.Models.Accomodation", "Accomodation")
                        .WithMany("Prices")
                        .HasForeignKey("AccomodationId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Dreamcatchers.Models.Profile", b =>
                {
                    b.HasOne("Dreamcatchers.Models.Security.ApplicationUser")
                        .WithOne("Profile")
                        .HasForeignKey("Dreamcatchers.Models.Profile", "ApplicationUserProfileId")
                        .HasPrincipalKey("Dreamcatchers.Models.Security.ApplicationUser", "ProfileId");
                });

            modelBuilder.Entity("Dreamcatchers.Models.ProfileTrip", b =>
                {
                    b.HasOne("Dreamcatchers.Models.Profile", "Profile")
                        .WithMany("Trips")
                        .HasForeignKey("ProfileId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Dreamcatchers.Models.Trip", "Trip")
                        .WithMany("Profiles")
                        .HasForeignKey("TripId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Dreamcatchers.Models.State", b =>
                {
                    b.HasOne("Dreamcatchers.Models.Country", "Country")
                        .WithMany("States")
                        .HasForeignKey("CountryId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Dreamcatchers.Models.Status", b =>
                {
                    b.HasOne("Dreamcatchers.Models.Accomodation", "Accomodation")
                        .WithMany("Statuses")
                        .HasForeignKey("AccomodationId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Dreamcatchers.Models.Trip", b =>
                {
                    b.HasOne("Dreamcatchers.Models.Accomodation", "Accomodation")
                        .WithMany("Trips")
                        .HasForeignKey("AccomodationId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<System.Guid>", b =>
                {
                    b.HasOne("Dreamcatchers.Models.Security.ApplicationRole")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<System.Guid>", b =>
                {
                    b.HasOne("Dreamcatchers.Models.Security.ApplicationUser")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<System.Guid>", b =>
                {
                    b.HasOne("Dreamcatchers.Models.Security.ApplicationUser")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<System.Guid>", b =>
                {
                    b.HasOne("Dreamcatchers.Models.Security.ApplicationRole")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Dreamcatchers.Models.Security.ApplicationUser")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("OpenIddict.OpenIddictToken<System.Guid>", b =>
                {
                    b.HasOne("OpenIddict.OpenIddictApplication<System.Guid>")
                        .WithMany("Tokens")
                        .HasForeignKey("ApplicationId");

                    b.HasOne("OpenIddict.OpenIddictAuthorization<System.Guid>")
                        .WithMany("Tokens")
                        .HasForeignKey("AuthorizationId");
                });
        }
    }
}
