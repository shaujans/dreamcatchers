using System;
using System.Collections.Generic;

namespace Dreamcatchers.Models
{
    public class BathroomConfig : BaseEntity<Int64>
    {
        public String Name { get; set; }
        public String Shortname { get; set; }
        public List<AccomodationBathroomConfig> Accomodations { get; set; }
    }
}