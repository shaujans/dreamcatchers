using System;
using System.Collections.Generic;

namespace Dreamcatchers.Models
{
    public class Price : BaseEntity<Int32>
    {
        public float PriceDay { get; set; }
        public float PriceWeek { get; set; }
        public float Cleaning { get; set; }
        public float Service { get; set; }
        public float Deposit { get; set; }


        public Int64 AccomodationId { get; set; }
        public Accomodation Accomodation { get; set; }

    }
}