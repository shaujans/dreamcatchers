using System;
using System.Collections.Generic;

namespace Dreamcatchers.Models
{
    public class AccomodationFacilityConfig
    {
        public Int64 AccomodationId { get; set; }
        public Accomodation Accomodation { get; set; }

        public Int64 FacilityConfigId { get; set; }
        public FacilityConfig FacilityConfig { get; set; }
    }
}
