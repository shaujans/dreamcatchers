using System;
using System.Collections.Generic;

namespace Dreamcatchers.Models
{
    public class FacilityConfig : BaseEntity<Int64>
    {
        public String Name { get; set; }
        public String Shortname { get; set; }

        public List<AccomodationFacilityConfig> Accomodations { get; set; }

        public Nullable<Int64> FacilityCategorieId { get; set; }
        public FacilityCategorie FacilityCategorie { get; set; }

    }
}