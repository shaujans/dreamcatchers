﻿using System;

namespace Dreamcatchers.Models
{
    public interface IBaseEntity<T>
    {
        T Id { get; set; }
        DateTime CreatedAt { get; set; }
        Nullable<DateTime> UpdatedAt { get; set; }
        Nullable<DateTime> DeletedAt { get; set; }
    }
}