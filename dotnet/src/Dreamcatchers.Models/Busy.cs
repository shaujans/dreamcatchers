using System;
using System.Collections.Generic;

namespace Dreamcatchers.Models
{
    public class Busy : BaseEntity<Int64>
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public Int64 AccomodationId { get; set; }
        public Accomodation Accomodation { get; set; }

    }
}