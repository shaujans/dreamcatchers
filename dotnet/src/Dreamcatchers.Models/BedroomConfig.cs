using System;
using System.Collections.Generic;

namespace Dreamcatchers.Models
{
    public class BedroomConfig : BaseEntity<Int64>
    {
        public String Name { get; set; }
        public String Shortname { get; set; }
        public Int16 MaxPersons { get; set; }

        public List<AccomodationBedroomConfig> Accomodations { get; set; }

    }
}