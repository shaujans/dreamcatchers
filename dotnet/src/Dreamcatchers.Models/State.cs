using System;
using System.Collections.Generic;

namespace Dreamcatchers.Models
{
    public class State : BaseEntity<Int64>
    {
        public String Name { get; set; }

        public Int64 CountryId { get; set; }
        public Country Country { get; set; }

        public List<City> Cities { get; set; }
    }
}