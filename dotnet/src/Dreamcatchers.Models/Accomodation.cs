using System;
using System.Collections.Generic;

namespace Dreamcatchers.Models
{
    public class Accomodation : BaseEntity<Int64>
    {
        public String Name { get; set; }
        public String Description { get; set; }
        public Int64 MaxPersons { get; set; }

        public Nullable<Int32> HouseTypeId { get; set; }
        public HouseType HouseType { get; set; }

        public Nullable<Int32> RentingTypeId { get; set; }
        public RentingType RentingType { get; set; }

        public Nullable<Int64> LocationId { get; set; }
        public Location Location { get; set; }

        public Nullable<Int64> ProfileId { get; set; }
        public Profile Profile { get; set; }

        public List<Booking> Bookings { get; set; }
        public List<Price> Prices { get; set; }
        public List<Trip> Trips { get; set; }
        public List<Status> Statuses { get; set; }
        public List<Busy> Busies { get; set; }

        public List<AccomodationBedroomConfig> BedroomConfigs { get; set; }
        public List<AccomodationBathroomConfig> BathroomConfigs { get; set; }
        public List<AccomodationFacilityConfig> FacilityConfigs { get; set; }
        public List<AccomodationMedia> Medias { get; set; }

    }
}