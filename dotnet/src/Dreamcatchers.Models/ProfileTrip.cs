using System;
using System.Collections.Generic;

namespace Dreamcatchers.Models
{
    public class ProfileTrip
    {
        public Int64 ProfileId { get; set; }
        public Profile Profile { get; set; }

        public Int64 TripId { get; set; }
        public Trip Trip { get; set; }
    }
}
