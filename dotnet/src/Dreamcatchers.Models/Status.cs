using System;
using System.Collections.Generic;

namespace Dreamcatchers.Models
{
    public class Status : BaseEntity<Int64>
    {
        public String Title { get; set; }
        public String Content { get; set; }
        public Int64 AccomodationId { get; set; }
        public Accomodation Accomodation { get; set; }

    }
}