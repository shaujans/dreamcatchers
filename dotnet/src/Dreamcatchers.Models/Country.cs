using System;
using System.Collections.Generic;

namespace Dreamcatchers.Models
{
    public class Country : BaseEntity<Int64>
    {
        public String Name { get; set; }
        public String Code { get; set; }
        public List<State> States { get; set; }
        

    }
}