using System;
using System.Collections.Generic;

namespace Dreamcatchers.Models
{
    public class Media : BaseEntity<Int64>
    {
        public String Name { get; set; }
        public String Url { get; set; }
        public Int16 Type { get; set; }
        public List<AccomodationMedia> Accomodations { get; set; }

    }
}