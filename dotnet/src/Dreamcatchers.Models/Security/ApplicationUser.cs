using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using OpenIddict;
using Dreamcatchers.Models;

namespace Dreamcatchers.Models.Security
{
    public class ApplicationUser : IdentityUser<Guid> 
    {
        public Nullable<Int64> ProfileId { get; set; }
        public Profile Profile { get; set; }
        public DateTime CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<DateTime> DeletedAt { get; set; }
    }
}