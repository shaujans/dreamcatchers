using System;
using System.Collections.Generic;

namespace Dreamcatchers.Models
{
    public class Location : BaseEntity<Int64>
    {
        public Double Lon { get; set; }
        public Double Lat { get; set; }
        public String Street { get; set; }
        public String Number { get; set; }
        public Int64 CityId { get; set; }
        public City City { get; set; }
        public List<Accomodation> Accomodations { get; set; }

    }
}