using System;
using System.Collections.Generic;

namespace Dreamcatchers.Models
{
    public class AccomodationBathroomConfig
    {
        public Int64 AccomodationId { get; set; }
        public Accomodation Accomodation { get; set; }

        public Int64 BathroomConfigId { get; set; }
        public BathroomConfig BathroomConfig { get; set; }
    }
}
