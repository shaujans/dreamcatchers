using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Dreamcatchers.Models;

namespace Dreamcatchers.Models.ViewModels
{
    public class AccomodationViewModel
    {
        public Accomodation Accomodation { get; set; }

    }
}