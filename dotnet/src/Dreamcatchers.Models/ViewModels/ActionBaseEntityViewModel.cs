using System;
using Dreamcatchers.Models;

namespace Dreamcatchers.Models.ViewModels
{
    public class ActionBaseEntityViewModel<T>: ActionViewModel
    {
        public BaseEntity<T> BaseEntity { get; set; }
    }
}