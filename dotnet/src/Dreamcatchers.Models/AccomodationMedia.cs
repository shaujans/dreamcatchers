using System;
using System.Collections.Generic;

namespace Dreamcatchers.Models
{
    public class AccomodationMedia
    {
        public Int64 AccomodationId { get; set; }
        public Accomodation Accomodation { get; set; }

        public Int64 MediaId { get; set; }
        public Media Media { get; set; }
    }
}
