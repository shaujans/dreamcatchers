using System;
using System.Collections.Generic;

namespace Dreamcatchers.Models
{
    public class City : BaseEntity<Int64>
    {
        public String Name { get; set; }
        public Int64 StateId { get; set; }
        public State State { get; set; }
        public List<Location> Locations { get; set; }

    }
}