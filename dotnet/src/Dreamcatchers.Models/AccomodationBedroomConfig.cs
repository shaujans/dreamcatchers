using System;
using System.Collections.Generic;

namespace Dreamcatchers.Models
{
    public class AccomodationBedroomConfig
    {
        public Int64 AccomodationId { get; set; }
        public Accomodation Accomodation { get; set; }

        public Int64 BedroomConfigId { get; set; }
        public BedroomConfig BedroomConfig { get; set; }
    }
}
