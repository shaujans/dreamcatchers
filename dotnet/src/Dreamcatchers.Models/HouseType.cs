using System;
using System.Collections.Generic;

namespace Dreamcatchers.Models
{
    public class HouseType : BaseEntity<Int32>
    {
        public String Name { get; set; }
        public String ShortName { get; set; }
        public List<Accomodation> Accomodations { get; set; }

    }
}