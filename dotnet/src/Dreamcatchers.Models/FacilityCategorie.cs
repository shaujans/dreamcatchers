using System;
using System.Collections.Generic;

namespace Dreamcatchers.Models
{
    public class FacilityCategorie : BaseEntity<Int64>
    {
        public String Name { get; set; }
        public List<FacilityConfig> FacilityConfigs { get; set; }

    }
}