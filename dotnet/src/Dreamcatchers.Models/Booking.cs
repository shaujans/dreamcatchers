using System;
using System.Collections.Generic;

namespace Dreamcatchers.Models
{
    public enum BookingStatus : byte{
        Unknown = 0,
        Pending = 1,
        Accepted = 2,
        Denied = 3,
        Canceled = 4
    }

    public class Booking : BaseEntity<Int32>
    {
        public DateTime Arrival { get; set; }
        public DateTime Departure { get; set; }
        public Decimal Price { get; set; }
        public BookingStatus Status { get; set; }

        public Int64 ProfileId { get; set; }
        public Profile Profile { get; set; }

        public Int64 AccomodationId { get; set; }
        public Accomodation Accomodation { get; set;}
        

    }
}