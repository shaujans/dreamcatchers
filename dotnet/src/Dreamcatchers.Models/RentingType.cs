using System;
using System.Collections.Generic;

namespace Dreamcatchers.Models
{
    public class RentingType : BaseEntity<Int32>
    {
        public String Name { get; set; }
        
        public String Shortname { get; set; }
        public List<Accomodation> Accomodations { get; set; }

    }
}