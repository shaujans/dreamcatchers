using System;
using System.Collections.Generic;

namespace Dreamcatchers.Models
{
    public enum GenderType : byte {
    Unknown = 0,
    Male = 1,
    Female = 2,
    NotApplicable = 9
    }
    
    public class Profile : BaseEntity<Int64>
    {


        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Nullable<DateTime> Birthday { get; set; }
        public GenderType Gender { get; set; }


        public List<Booking> Bookings { get; set; }
        public List<Accomodation> Accomodations { get; set; }
        public List<ProfileTrip> Trips { get; set; }
    }
}