using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Dreamcatchers.Db;
using Dreamcatchers.Models;
using Dreamcatchers.Models.Utilities;
using Dreamcatchers.Models.Security;

namespace  Dreamcatchers.WWW.Areas.Backoffice.Controllers 
{
    [Area("Backoffice")]
    public abstract class BaseController : Controller 
    {
        public ApplicationDbContext ApplicationDbContext { get; set; }
        public UserManager<ApplicationUser>  ApplicationUserManager  { get; set; }
        public RoleManager<ApplicationRole>  ApplicationRoleManager  { get; set; }
        
        public BaseController() 
        {
        }

        public BaseController([FromServices]ApplicationDbContext applicationDbContext) 
        {
            ApplicationDbContext = applicationDbContext;
        }

        public BaseController([FromServices]ApplicationDbContext applicationDbContext, [FromServices]UserManager<ApplicationUser>  ApplicationUserManager, [FromServices]RoleManager<ApplicationRole>  ApplicationRoleManager) 
        {
            ApplicationDbContext = applicationDbContext;
        }

        public void AddAlert(AlertType alertType, string message, bool dismissable)
        {
            var alerts = TempData.ContainsKey("SRVALERT")
                ? (List<Alert>)TempData["SRVALERT"]
                : new List<Alert>();
    
            alerts.Add(new Alert
            {
                Type = alertType,
                Message = message,
                Dismissable = dismissable
            });
    
            TempData["SRVALERT"] = alerts;
        }

        public void AddAlert(Alert alert)
        {
            var alerts = TempData.ContainsKey("SRVALERT")
                ? (List<Alert>)TempData["SRVALERT"]
                : new List<Alert>();
    
            if(alert != null)
            {
                alerts.Add(alert);
            }
    
            TempData["SRVALERT"] = alerts;
        }
    }
}