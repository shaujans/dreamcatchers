using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Dreamcatchers.Db;
using Dreamcatchers.Models;
using Dreamcatchers.Models.Security;

namespace  Dreamcatchers.WWW.Areas.Backoffice.Controllers 
{
    [Area("Backoffice")]
    public class HomeController : BaseController 
    {
        public HomeController():base()
        {
        }

        public IActionResult Index() 
        {
            return View();
        }
    }
}