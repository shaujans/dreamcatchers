using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Dreamcatchers.Db;
using Dreamcatchers.Models;
using Dreamcatchers.Models.ViewModels;
using Dreamcatchers.Models.Security;

namespace  Dreamcatchers.WWW.Areas.Backoffice.ViewComponents 
{
    public abstract class BaseViewComponent : ViewComponent 
    {
        public ApplicationDbContext ApplicationDbContext { get; set; }
        public UserManager<ApplicationUser>  ApplicationUserManager  { get; set; }
        public RoleManager<ApplicationRole>  ApplicationRoleManager  { get; set; }
        
        public BaseViewComponent() 
        {
        }

        public BaseViewComponent([FromServices]ApplicationDbContext applicationDbContext) 
        {
            ApplicationDbContext = applicationDbContext;
        }

        public BaseViewComponent([FromServices]ApplicationDbContext applicationDbContext, [FromServices]UserManager<ApplicationUser>  ApplicationUserManager, [FromServices]RoleManager<ApplicationRole>  ApplicationRoleManager) 
        {
            ApplicationDbContext = applicationDbContext;
        }
    }
}