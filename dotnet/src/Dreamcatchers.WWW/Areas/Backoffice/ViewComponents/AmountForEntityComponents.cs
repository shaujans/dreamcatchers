using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Dreamcatchers.Db;
using Dreamcatchers.Models;
using Dreamcatchers.Models.ViewModels;
using Dreamcatchers.Models.Security;

namespace  Dreamcatchers.WWW.Areas.Backoffice.ViewComponents 
{
    [ViewComponent(Name="AmountForEntity")]
    public class AmountForEntityViewComponent : BaseViewComponent 
    {
        public AmountForEntityViewComponent(ApplicationDbContext applicationDbContext) : base(applicationDbContext) 
        {
        }

        public async Task<IViewComponentResult> InvokeAsync(string entityType)
        {
            var viewModel = await GetAmountForEntityAsync(entityType);
            return View(viewModel);
        }

        private Task<AmountForEntityViewModel> GetAmountForEntityAsync(string entityType)
        {
            return Task.FromResult(GetAmountForEntity(entityType));
        }

        private AmountForEntityViewModel GetAmountForEntity(string entityType)
        {
            var amount = 0;
            entityType = entityType;
            var name = entityType;
            var pluralizeName = "Entities";

            switch(entityType)
            {
                case "Accomodation":
                    amount = ApplicationDbContext.Accomodations.AsEnumerable().Count();
                    entityType = "Accomodation";
                    name = "Accomodation";
                    pluralizeName = "Accomodations";
                    break;
                
            }

            var viewModel = new AmountForEntityViewModel()
            {
                Amount = amount,
                EntityType = entityType,
                Name = name,
                PluralizeName = pluralizeName
            };

            return viewModel;
        }
    }
}