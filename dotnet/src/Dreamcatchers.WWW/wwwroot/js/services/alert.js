/*
 * Programmed by Roosens Jef
 * Version: 1.0
 * Last updated: 2016-12-27
 * Education license: only use in educationel institutions!
 */
var AlertService = (function ($) {

    var _alertContainerElement;

    function AlertService(alertContainerSelector) {
        _alertContainerElement = $(alertContainerSelector);
    }

    AlertService.prototype.showAlert = function (type, message) {
        var content = ''
        + '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'
        + '<div class="alert ' + convertAlertTypeToBootstrapAlertType(type) + '">'
        + '<p>' + message + '</p>'
        + '</div>'
        + '</div>';
        _alertContainerElement.append(content);

        var alertElement = _alertContainerElement.children().last();

        window.setTimeout(function () {
            alertElement.fadeOut('slow', function () {
                $(this).remove();
            });
        }, 3000);
		
    };

    AlertService.prototype.convertAlertTypeToBootstrapAlertType = function (type, message) {
        switch (alertType) {
            case 'Success': default: return 'alert-success';
            case 'Info': return 'alert-info';
            case 'Warning': return 'alert-warning';
            case 'Error': return 'alert-danger';
        }
    };

    return AlertService;
	
})(jQuery);