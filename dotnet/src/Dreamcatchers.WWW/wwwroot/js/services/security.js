/*
 * Programmed by Roosens Jef
 * Version: 1.0
 * Last updated: 2016-12-27
 * Education license: only use in educationel institutions!
 */
var SecurityService = (function ($) {

    function SecurityService() {
    }

    SecurityService.prototype.getAntiForgeryToken = function (containerSelector) {
      var tokenWindow = window;
      var tokenName = "__RequestVerificationToken";
      var tokenField = $(containerSelector).find("input[type='hidden'][name='" + tokenName + "']");
      if (tokenField.length == 0) { return null; }
      else {
          return {
              name: tokenName,
              value: tokenField.val()
          };
      }
    };

    return SecurityService;
	
})(jQuery);