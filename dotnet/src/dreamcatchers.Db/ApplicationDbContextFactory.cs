using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Npgsql.EntityFrameworkCore.PostgreSQL;
using Dreamcatchers.Models;
using Dreamcatchers.Models.Security;

namespace Dreamcatchers.Db
{
    public class ApplicationDbContextFactory : IDbContextFactory<ApplicationDbContext>
    {
        public ApplicationDbContext Create(DbContextFactoryOptions options)
        {
            var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
            builder.UseNpgsql("User ID=originaljef;Password=postgres;Host=localhost;Port=5432;Database=dreamcatchers;Pooling=true;");
            return new ApplicationDbContext(builder.Options);
        }
    }
}