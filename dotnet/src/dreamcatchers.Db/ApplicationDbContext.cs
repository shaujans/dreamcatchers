using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using OpenIddict;
using Npgsql.EntityFrameworkCore.PostgreSQL;
using Dreamcatchers.Models;
using Dreamcatchers.Models.Security;

namespace Dreamcatchers.Db
{
    public class ApplicationDbContext : OpenIddictDbContext<ApplicationUser, ApplicationRole, Guid>
    {
        public DbSet<Profile> Profiles { get; set; }
        public DbSet<Accomodation> Accomodations{ get; set; }
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<Busy> Busies { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<HouseType> HouseTypes { get; set; }
        public DbSet<Price> Prices { get; set; }
        public DbSet<ProfileTrip> ProfileTrips { get; set; }
        public DbSet<RentingType> RentingTypes { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<Trip> Trips { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<BedroomConfig> BedroomConfigs { get; set; }
        public DbSet<BathroomConfig> BathroomConfigs { get; set; }
        public DbSet<FacilityConfig> FacilityConfigs { get; set; }
        public DbSet<Media> Medias { get; set; }
        public DbSet<FacilityCategorie> FacilityCategories { get; set; }
        public DbSet<AccomodationBedroomConfig> AccomodationBedroomConfigs { get; set; }
        public DbSet<AccomodationBathroomConfig> AccomodationBathroomConfigs { get; set; }
        public DbSet<AccomodationFacilityConfig> AccomodationFacilityConfigs { get; set; }
        public DbSet<AccomodationMedia> AccomodationMedias { get; set; }
        
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
             builder.HasPostgresExtension("uuid-ossp");
            base.OnModelCreating(builder);

            //Model: ApplicationUser
            builder.Entity<ApplicationUser>()
                .Property(u => u.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<ApplicationUser>()
                .Property(u => u.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();

            builder.Entity<ApplicationUser>()
                .HasOne(u => u.Profile)
                .WithOne()
                .HasPrincipalKey<ApplicationUser>(u => u.ProfileId);

            // Model: Profile
            builder.Entity<Profile>()
                .HasKey(o => o.Id);

            builder.Entity<Profile>()
                .Property(o => o.FirstName)
                .HasMaxLength(255)
                .IsRequired();
            
            builder.Entity<Profile>()
                .Property(o => o.LastName)
                .HasMaxLength(255)
                .IsRequired();

            builder.Entity<Profile>()
                .Property(o => o.Gender)
                .IsRequired();

            builder.Entity<Profile>()
                .Property(o => o.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<Profile>()
                .Property(o => o.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();

            // Model : Accomodations
            builder.Entity<Accomodation>()
                .HasKey(o => o.Id);

            builder.Entity<Accomodation>()
                .Property(o => o.Name)
                .HasMaxLength(255)
                .IsRequired();

            builder.Entity<Accomodation>()
                .Property(o => o.Description)
                .IsRequired();

            builder.Entity<Accomodation>()
                .Property(o => o.MaxPersons)
                .IsRequired();
             

            // Model : Trips
            builder.Entity<Trip>()
                .HasKey(o => o.Id);

            builder.Entity<Trip>()
                .Property(o => o.From)
                .IsRequired();

            builder.Entity<Trip>()
                .Property(o => o.To)
                .IsRequired();

            // Model: Booking
            builder.Entity<Booking>()
                .HasKey(o => o.Id);

            builder.Entity<Booking>()
                .Property(o => o.Arrival)
                .IsRequired();

            builder.Entity<Booking>()
                .Property(o => o.Departure)
                .IsRequired();

            builder.Entity<Booking>()
                .Property(o => o.Price)
                .IsRequired();

            builder.Entity<Booking>()
                .Property(o => o.Status)
                .IsRequired();

            builder.Entity<Booking>()
                .Property(o => o.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<Booking>()
                .Property(o => o.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();

            // Model: Location
            builder.Entity<Location>()
                .HasKey(o => o.Id);

            builder.Entity<Location>()
                .Property(o => o.Lon)
                .HasMaxLength(127)
                .IsRequired();

            builder.Entity<Location>()
                .Property(o => o.Lat)
                .HasMaxLength(127)
                .IsRequired();
                
            // Model: Status
            builder.Entity<Status>()
                .HasKey(o => o.Id);

            builder.Entity<Status>()
                .Property(o => o.Title)
                .HasMaxLength(255)
                .IsRequired();

            builder.Entity<Status>()
                .Property(o => o.Content)
                .IsRequired();

            // Model: Busy
            builder.Entity<Busy>()
                .HasKey(o => o.Id);

            builder.Entity<Busy>()
                .Property(o => o.From)
                .IsRequired();

            builder.Entity<Busy>()
                .Property(o => o.To)
                .IsRequired();

            // Model: HouseType
            builder.Entity<HouseType>()
                .HasKey(o => o.Id);

            builder.Entity<HouseType>()
                .Property(o => o.Name)
                .HasMaxLength(255)
                .IsRequired();

            builder.Entity<HouseType>()
                .Property(o => o.ShortName)
                .HasMaxLength(127)
                .IsRequired();

            // Model: RentingType
            builder.Entity<RentingType>()
                .HasKey(o => o.Id);

            builder.Entity<RentingType>()
                .Property(o => o.Name)
                .HasMaxLength(255)
                .IsRequired();

            builder.Entity<RentingType>()
                .Property(o => o.Shortname)
                .HasMaxLength(127)
                .IsRequired();

            // Model: Price
            builder.Entity<Price>()
                .HasKey(o => o.Id);

            builder.Entity<Price>()
                .Property(o => o.PriceDay)
                .IsRequired();

            builder.Entity<Price>()
                .Property(o => o.PriceWeek)
                .IsRequired();

            builder.Entity<Price>()
                .Property(o => o.Cleaning)
                .IsRequired();

            builder.Entity<Price>()
                .Property(o => o.Service)
                .IsRequired();

            builder.Entity<Price>()
                .Property(o => o.Deposit)
                .IsRequired();

            // Model: BedroomConfig
            builder.Entity<BedroomConfig>()
                .HasKey( o => o.Id );

            builder.Entity<BedroomConfig>()
                .Property( o => o.Name)
                .IsRequired();
            
            builder.Entity<BedroomConfig>()
                .Property( o => o.Shortname )
                .HasMaxLength(127);

            builder.Entity<BedroomConfig>()
                .Property( o => o.MaxPersons)
                .IsRequired();

            // Model: BathroomConfig
            builder.Entity<BathroomConfig>()
                .HasKey( o => o.Id );

            builder.Entity<BathroomConfig>()
                .Property( o => o.Name)
                .IsRequired();

            builder.Entity<BedroomConfig>()
                .Property( o => o.Shortname )
                .HasMaxLength(127);

            // Model: FacilityConfig
            builder.Entity<FacilityConfig>()
                .HasKey( o => o.Id );

            builder.Entity<FacilityConfig>()
                .Property( o => o.Name)
                .IsRequired();

            builder.Entity<FacilityConfig>()
                .Property( o => o.Shortname )
                .HasMaxLength(127);

            // Model: FacilityCategorie
            builder.Entity<FacilityCategorie>()
                .HasKey( o => o.Id );
            
            builder.Entity<FacilityCategorie>()
                .Property( o => o.Name)
                .IsRequired();
                
            // Relationship between Profile and Booking: 0 to many (n)
            builder.Entity<Booking>()
                .HasOne(b => b.Profile)
                .WithMany(p => p.Bookings)
                .HasForeignKey(b => b.ProfileId);

             // Relationship between Profile and Accomodiation: 0 to many (n)
            builder.Entity<Accomodation>()
                .HasOne(b => b.Profile)
                .WithMany(p => p.Accomodations)
                .HasForeignKey(b => b.ProfileId);

            // Relationship between HouseType and Accomodiation: 0 to many (n)
            builder.Entity<Accomodation>()
                .HasOne(b => b.HouseType)
                .WithMany(p => p.Accomodations)
                .HasForeignKey(b => b.HouseTypeId);

            // Relationship between RentingType and Accomodiation: 0 to many (n)
            builder.Entity<Accomodation>()
                .HasOne(b => b.RentingType)
                .WithMany(p => p.Accomodations)
                .HasForeignKey(b => b.RentingTypeId);

            // Relationship between Accomodation and Trips: 0 to many (n)
            builder.Entity<Trip>()
                .HasOne(b => b.Accomodation)
                .WithMany(p => p.Trips)
                .HasForeignKey(b => b.AccomodationId);

            // Relationship between Accomodation and Bookings: 0 to many (n)
            builder.Entity<Booking>()
                .HasOne(b => b.Accomodation)
                .WithMany(p => p.Bookings)
                .HasForeignKey(b => b.AccomodationId);

            // Relationship between Accomodation and Status: 0 to many (n)
            builder.Entity<Status>()
                .HasOne(b => b.Accomodation)
                .WithMany(p => p.Statuses)
                .HasForeignKey(b => b.AccomodationId);

            // Relationship between Accomodation and Busy: 0 to many (n)
            builder.Entity<Busy>()
                .HasOne(b => b.Accomodation)
                .WithMany(p => p.Busies)
                .HasForeignKey(b => b.AccomodationId);

            // Relationship between Accomodation and Price: 0 to many (n)
            builder.Entity<Price>()
                .HasOne(b => b.Accomodation)
                .WithMany(p => p.Prices)
                .HasForeignKey(b => b.AccomodationId);

            // Relationship between FacilityConfig and FacilityCategorie: 0 to many (n)
            builder.Entity<FacilityConfig>()
                .HasOne(b => b.FacilityCategorie)
                .WithMany(p => p.FacilityConfigs)
                .HasForeignKey(b => b.FacilityCategorieId);

            // Relationship between Accomodation and Location: 0 to many (n)
            builder.Entity<Accomodation>()
                .HasOne(b => b.Location)
                .WithMany(p => p.Accomodations)
                .HasForeignKey(b => b.LocationId);

            // Relationship between Location and City: 0 to many (n)
            builder.Entity<Location>()
                .HasOne(b => b.City)
                .WithMany(p => p.Locations)
                .HasForeignKey(b => b.CityId);

            // Relationship between City and State: 0 to many (n)
            builder.Entity<City>()
                .HasOne(b => b.State)
                .WithMany(p => p.Cities)
                .HasForeignKey(b => b.StateId);
            
            // Relationship between State and Country: 0 to many (n)
            builder.Entity<State>()
                .HasOne(b => b.Country)
                .WithMany(p => p.States)
                .HasForeignKey(b => b.CountryId);

            // Many-To-Many in EF7 (10-10-2016)
            // Profile can have many Trips
            // Trip can have many Profiles
            builder.Entity<ProfileTrip>()
                .HasKey(t => new { t.ProfileId, t.TripId });

            builder.Entity<ProfileTrip>()
                .HasOne(pt => pt.Profile)
                .WithMany(p => p.Trips)
                .HasForeignKey(pt => pt.ProfileId);

            builder.Entity<ProfileTrip>()
                .HasOne(pt => pt.Trip)
                .WithMany(p => p.Profiles)
                .HasForeignKey(pt => pt.TripId);

            // Many-To-Many in EF7 (10-10-2016)
            // Accomodation can have many Bedroom Configurations
            // Bedroom Configuration can have many Accomodations
            builder.Entity<AccomodationBedroomConfig>()
                .HasKey(t => new { t.AccomodationId, t.BedroomConfigId });

            builder.Entity<AccomodationBedroomConfig>()
                .HasOne(pt => pt.Accomodation)
                .WithMany(p => p.BedroomConfigs)
                .HasForeignKey(pt => pt.AccomodationId);

            builder.Entity<AccomodationBedroomConfig>()
                .HasOne(pt => pt.BedroomConfig)
                .WithMany(p => p.Accomodations)
                .HasForeignKey(pt => pt.BedroomConfigId);

            // Many-To-Many in EF7 (10-10-2016)
            // Accomodation can have many Bathroom Configurations
            // Bathroom Configuration can have many Accomodations
            builder.Entity<AccomodationBathroomConfig>()
                .HasKey(t => new { t.AccomodationId, t.BathroomConfigId });

            builder.Entity<AccomodationBathroomConfig>()
                .HasOne(pt => pt.Accomodation)
                .WithMany(p => p.BathroomConfigs)
                .HasForeignKey(pt => pt.AccomodationId);

            builder.Entity<AccomodationBathroomConfig>()
                .HasOne(pt => pt.BathroomConfig)
                .WithMany(p => p.Accomodations)
                .HasForeignKey(pt => pt.BathroomConfigId);

            // Many-To-Many in EF7 (10-10-2016)
            // Accomodation can have many Facility Configurations
            // Facility Configuration can have many Accomodations
            builder.Entity<AccomodationFacilityConfig>()
                .HasKey(t => new { t.AccomodationId, t.FacilityConfigId });

            builder.Entity<AccomodationFacilityConfig>()
                .HasOne(pt => pt.Accomodation)
                .WithMany(p => p.FacilityConfigs)
                .HasForeignKey(pt => pt.AccomodationId);

            builder.Entity<AccomodationFacilityConfig>()
                .HasOne(pt => pt.FacilityConfig)
                .WithMany(p => p.Accomodations)
                .HasForeignKey(pt => pt.FacilityConfigId);

            // Many-To-Many in EF7 (10-10-2016)
            // Accomodation can have many Medias
            // Media can have many Accomodations
            builder.Entity<AccomodationMedia>()
                .HasKey(t => new { t.AccomodationId, t.MediaId });

            builder.Entity<AccomodationMedia>()
                .HasOne(pt => pt.Accomodation)
                .WithMany(p => p.Medias)
                .HasForeignKey(pt => pt.AccomodationId);

            builder.Entity<AccomodationMedia>()
                .HasOne(pt => pt.Media)
                .WithMany(p => p.Accomodations)
                .HasForeignKey(pt => pt.MediaId);

            
        }
    }
}