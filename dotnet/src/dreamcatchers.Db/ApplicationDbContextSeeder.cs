using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Bogus;
using Bogus.DataSets;
using Bogus.Extensions;
using Dreamcatchers.Models;
using Dreamcatchers.Models.Security;

namespace Dreamcatchers.Db
{
    public class ApplicationDbContextSeeder 
    {
        public static async void Initialize(IServiceProvider serviceProvider)
        {
            var userManager = serviceProvider.GetService<UserManager<ApplicationUser>>();

            using(var context = serviceProvider.GetService<ApplicationDbContext>()) 
            {
                // Random instance
                var random = new Random();


                // Generate Users
                if(!userManager.Users.Any())
                {
                    ApplicationUser user = new ApplicationUser();
                    user.UserName = "JefShaun";
                    user.Email = "jef.shaun@dreamcatchers.be";

                    Task<IdentityResult> result = userManager.CreateAsync(user, "Azerty123");
                }

                // Generate Profile
                if(!context.Profiles.Any())
                {
                    var profileSkeleton = new Faker<Dreamcatchers.Models.Profile>()
                        .RuleFor(o => o.FirstName, f => f.Name.FirstName())
                        .RuleFor(o => o.LastName, f => f.Name.LastName())
                        .RuleFor(o => o.Birthday, f => GenerateDateTime(1960, 1998, 1, 13, 1, 32))
                        .RuleFor(o => o.Gender, f => f.PickRandom<GenderType>())
                        .FinishWith((f, o) =>
                        {
                            Console.WriteLine("Profile created with Bogus {0}:)", o.Id);
                        });
                    
                    var profiles = new List<Profile>();
                    
                    for (var i = 0; i < 100; i++)
                    {
                        var profile = profileSkeleton.Generate();
                        profiles.Add(profile);
                    }

                    context.Profiles.AddRange(profiles);
                    await context.SaveChangesAsync();
                }

                // Generate Bedroom Config
                if(!context.BedroomConfigs.Any())
                {
                    context.BedroomConfigs.AddRange(new List<BedroomConfig>
                    {
                        new BedroomConfig { Name = "Eenpersoonsbed", Shortname = "eenpersoonsbed", MaxPersons = 1},
                        new BedroomConfig { Name = "Tweepersoonsbed", Shortname = "tweepersoonsbed", MaxPersons = 2},
                        new BedroomConfig { Name = "Twee Eenpersoons bedden", Shortname = "tweeenpersoonsbedden", MaxPersons = 2},
                        new BedroomConfig { Name = "Stapelbed", Shortname = "stapelbed", MaxPersons = 2},
                        new BedroomConfig { Name = "Tweepersoonsbed en eenpersoonsbed", Shortname = "tweepersoonsbedeneenpersoonsbed", MaxPersons = 2},
                        new BedroomConfig { Name = "Super mega deluxe round fucking machine bed ", Shortname = "supermegadeluxeroundfuckingmachinebed", MaxPersons = 6}
                    });
                    await context.SaveChangesAsync();
                }

                // Generate Bathroom Config
                if(!context.BathroomConfigs.Any())
                {
                    context.BathroomConfigs.AddRange(new List<BathroomConfig>
                    {
                        new BathroomConfig { Name = "Toilet", Shortname = "toilet" },
                        new BathroomConfig { Name = "Douche", Shortname = "douche" },
                        new BathroomConfig { Name = "Badkuip", Shortname = "badkuip" },
                        new BathroomConfig { Name = "Douche en toilet", Shortname = "doucheentoilet" },
                        new BathroomConfig { Name = "Badkuip en toilet", Shortname = "badkuipentoilet" }
                    });
                    await context.SaveChangesAsync();
                }

                // Generate Facility Config
                if(!context.FacilityConfigs.Any())
                {
                    context.FacilityConfigs.AddRange(new List<FacilityConfig>
                    {
                        new FacilityConfig { Name = "Wifi", Shortname = "wifi" },
                        new FacilityConfig { Name = "Bedraad internet", Shortname = "bedraadinternet" },
                        new FacilityConfig { Name = "Microgolfoven", Shortname = "microgolfoven" },
                        new FacilityConfig { Name = "Koelkast", Shortname = "koelkast" },
                        new FacilityConfig { Name = "Airco", Shortname = "Airco" },
                        new FacilityConfig { Name = "Koffiezetapparaat", Shortname = "koffiezetapparaat" },
                        new FacilityConfig { Name = "Televisie", Shortname = "televisie" },
                        new FacilityConfig { Name = "Strijkplank en -ijzer", Shortname = "strijkplankenijzer" },
                        new FacilityConfig { Name = "Huisdieren toegestaan", Shortname = "huisdierentoegestaan" },
                        new FacilityConfig { Name = "Gratis parkeerplaats", Shortname = "gratisparkeerplaats" },
                        new FacilityConfig { Name = "Droogkast", Shortname = "droogkast" },
                        new FacilityConfig { Name = "Wasmachine", Shortname = "wasmachine" },
                        new FacilityConfig { Name = "Vaatwasmachine", Shortname = "vaatwasmachine" }

                    });
                    await context.SaveChangesAsync();
                }

                // Generate Media
                if(!context.Medias.Any())
                {
                    context.Medias.AddRange(new List<Media>
                    {
                        new Media { Name = "afbeelding x", Url = "https://unsplash.it/1200/800/?random", Type = 1}
                    });
                    await context.SaveChangesAsync();
                }

                // Generate House Type
                if(!context.HouseTypes.Any())
                {
                    context.HouseTypes.AddRange(new List<HouseType>
                    {
                        new HouseType { Name = "Volledige woning", ShortName ="volledigewoning" },
                        new HouseType { Name = "Eigen kamer", ShortName = "eigenkamer" },
                        new HouseType { Name = "Gedeelde kamer", ShortName = "gedeeldekamer"}
                    });
                    await context.SaveChangesAsync();
                }

                // Generate Countries
                if(!context.Countries.Any())
                {
                    context.Countries.AddRange(new List<Country>
                    {
                        new Country { Name = "België" , Code = "BE" },
                        new Country { Name = "Nederlands" , Code = "NL" },
                        new Country { Name = "Frankrijk" , Code = "F" },
                        new Country { Name = "Duitsland" , Code = "D" },
                        new Country { Name = "Spanje" , Code = "ES" }
                    });
                    await context.SaveChangesAsync();
                }

                // Generate States
                if(!context.States.Any())
                {
                    context.States.AddRange(new List<State>
                    {
                        new State { Name = "Oost-Vlaanderen", CountryId = 1 },
                        new State { Name = "West-Vlaanderen", CountryId = 1 },
                        new State { Name = "Antwerpen", CountryId = 1 },
                        new State { Name = "Vlaams Brabant", CountryId = 1 },
                        new State { Name = "Limburg", CountryId = 1 },
                        new State { Name = "Walonië", CountryId = 1 }

                    });
                    await context.SaveChangesAsync();
                }

                //Generate Cities
                if(!context.Cities.Any())
                {
                    context.Cities.AddRange(new List<City>
                    {
                        new City { Name = "Leuven", StateId = 4},
                        new City { Name = "Namen", StateId = 6},
                        new City { Name = "Anderlecht", StateId = 4},
                        new City { Name = "Brugge", StateId = 2},
                        new City { Name = "Schaarbeek", StateId = 4},
                        new City { Name = "Brussel", StateId = 4},
                        new City { Name = "Luik", StateId = 6},
                        new City { Name = "Charleroi", StateId = 6},
                        new City { Name = "Gent", StateId = 1},
                        new City { Name = "Antwerpen", StateId = 3},
                        new City { Name = "Hasselt", StateId = 5},
                        new City { Name = "Eeklo", StateId = 1},
                        new City { Name = "Molenbeek", StateId = 4},

                    });
                    await context.SaveChangesAsync();
                }

                //Generate Locations
                if(!context.Locations.Any())
                {
                    var lorem = new Bogus.DataSets.Address();
                    var locationSkeleton = new Faker<Dreamcatchers.Models.Location>()
                        .RuleFor(o => o.Lon, f => lorem.Longitude())
                        .RuleFor(o => o.Lat, f => lorem.Latitude())
                        .RuleFor(o => o.Street, f => lorem.StreetName())
                        .RuleFor(o => o.Number, f => lorem.BuildingNumber())
                        .FinishWith((f, o) =>
                        {
                            Console.WriteLine("Location created with Bogus {0}:)", o.Id);
                        });
                    
                    var locations = new List<Location>();
                    var cities = context.Cities.ToList();

                    for (var i = 0; i < 12; i++)
                    {
                        var location = locationSkeleton.Generate();
                        location.CityId = cities[random.Next(cities.Count - 1)].Id;
                        locations.Add(location);
                    }

                    context.Locations.AddRange(locations);
                    await context.SaveChangesAsync();

                }

                //Generate Accomodations
                if(!context.Accomodations.Any())
                {
                    var lorem = new Bogus.DataSets.Lorem(locale: "nl");
                    var accomodationSkeleton = new Faker<Dreamcatchers.Models.Accomodation>()
                        .RuleFor( o => o.Name, f => lorem.Word())
                        .RuleFor ( o => o.Description, f => String.Join(" ", lorem.Words(5)))
                        .RuleFor (o => o.MaxPersons, f => lorem.Random.Number(1,10))
                        .FinishWith((f, o) =>
                        {
                            Console.WriteLine("Accomodation created with Bogus {0}:)", o.Id);
                        });
                    
                    var accomodations = new List<Accomodation>();
                    var housetypes = context.HouseTypes.ToList();
                    var locations = context.Locations.ToList();
                    var profiles = context.Profiles.ToList();

                    for (var i = 0; i < 60; i ++)
                    {
                        var accomodation = accomodationSkeleton.Generate();
                    accomodation.HouseTypeId = housetypes[random.Next(housetypes.Count - 1)].Id;
                    accomodation.LocationId = locations[random.Next(locations.Count - 1)].Id;
                    accomodation.ProfileId = profiles[i].Id;
                    accomodations.Add(accomodation);
                    }

                    context.Accomodations.AddRange(accomodations);
                    await context.SaveChangesAsync();
                }

                //Generate Bookings
                if(!context.Bookings.Any())
                {
                    var lorem = new Bogus.DataSets.Finance();
                    var bookingSkeleton = new Faker<Dreamcatchers.Models.Booking>()
                        .RuleFor( o => o.Arrival, f => GenerateDateTime(2015, 2018, 1, 13, 1, 32))
                        .RuleFor ( o => o.Departure, f => GenerateDateTime(2015, 2018, 1 , 13, 1, 32))
                        .RuleFor (o => o.Price, f => lorem.Amount())
                        .RuleFor (o => o.Status, f => f.PickRandom<BookingStatus>())
                        .FinishWith((f, o) =>
                        {
                            Console.WriteLine("Booking created with Bogus {0}:)", o.Id);
                        });
                    
                    var bookings = new List<Booking>();
                    var profiles = context.Profiles.ToList();
                    var accomodations = context.Accomodations.ToList();

                    for (var i = 0; i < 300; i++)
                    {
                        var booking = bookingSkeleton.Generate();
                        booking.ProfileId = profiles[random.Next(profiles.Count -1)].Id;
                        booking.AccomodationId = accomodations[random.Next(profiles.Count -1)].Id;
                        bookings.Add(booking);
                    }

                    context.Bookings.AddRange(bookings);
                    await context.SaveChangesAsync();
                }

                //Generate Trips
                if(!context.Trips.Any())
                {
                    var lorem = new Bogus.DataSets.Finance();
                    var tripSkeleton = new Faker<Dreamcatchers.Models.Trip>()
                        .RuleFor( o => o.From, f => GenerateDateTime(2015, 2018, 1, 13, 1, 32))
                        .RuleFor ( o => o.To, f => GenerateDateTime(2015, 2018, 1 , 13, 1, 32))
                        .RuleFor (o => o.Price, f => lorem.Amount())
                        .FinishWith((f, o) =>
                        {
                            Console.WriteLine("Trip created with Bogus {0}:)", o.Id);
                        });
                    var trips = new List<Trip>();
                    var accomodations = context.Accomodations.ToList();

                    for(var i = 0; i < 300; i++)
                    {
                        var trip = tripSkeleton.Generate();
                        trip.AccomodationId = accomodations[random.Next(accomodations.Count -1)].Id;
                        trips.Add(trip);
                    }

                    context.Trips.AddRange(trips);
                    await context.SaveChangesAsync();
                }
            }
            
        }

            private static DateTime GenerateDateTime(int yFrom, int yTo, int mFrom, int mTo, int dFrom, int dTo) {
            var random = new Random();
            try
            {
                return new DateTime(random.Next(yFrom, yTo), random.Next(mFrom, mTo), random.Next(dFrom, dTo));
            }
            catch(Exception ex) {
                return GenerateDateTime(yFrom, yTo, mFrom, mTo, dFrom, dTo);
            }
        }


    }
}